
public abstract class AbstractDuckFactory {
	abstract public IQuackable createredHeadDuck();
	abstract public IQuackable createDuck();
	abstract public IQuackable createRubberDuck();

}
