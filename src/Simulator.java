
public class Simulator {
	public static void main(String[] args){
		AbstractDuckFactory fact = new CountedDuckFactory();
		IQuackable d1 = fact.createDuck();
		IQuackable d2 = fact.createredHeadDuck();
		IQuackable d3 = fact.createRubberDuck();
		IQuackable d4 = new GooseAdapter(new Goose());
		simulate(d1);
		simulate(d2);
		simulate(d3);
		simulate(d4);
		System.out.println("ho sentito" + QuackCounter.getNumberOfQuack() + "quack");
	}
	public static void simulate(IQuackable q){
		q.Quack();
	}
}