
public class CountedDuckFactory extends AbstractDuckFactory {

	@Override
	public IQuackable createDuck() {
		// TODO Auto-generated method stub
		return new QuackCounter(new Duck());
	}

	@Override
	public IQuackable createRubberDuck() {
		// TODO Auto-generated method stub
		return new QuackCounter(new RubberDuck());
		}

	@Override
	public IQuackable createredHeadDuck() {
		// TODO Auto-generated method stub
		return new QuackCounter(new redHeadDuck());
		}

}
