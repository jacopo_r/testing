
public class QuackCounter implements IQuackable {
	IQuackable quacker;
	static long numberOfQuack = 0;
	
	public QuackCounter(IQuackable quacker){
		this.quacker = quacker;
	}
	@Override
	public void Quack() {
		numberOfQuack++;
		quacker.Quack();
	}
	public static long getNumberOfQuack(){
		return numberOfQuack;
	}

}
